var btnOpenPopupAddCrypto = document.getElementById('btn_popup_add_crypto')
var layoutPopupAddCrypto = document.getElementById('layout_add_crypto')
var popupAddCrypto = document.getElementById('popup_add_crypto')
var noClose = false;

btnOpenPopupAddCrypto.addEventListener('click', openPopupAddCrypto)
popupAddCrypto.addEventListener('click', noClosePopupAddCrypto)
layoutPopupAddCrypto.addEventListener('click', closePopupAddCrypto)

function openPopupAddCrypto () {
    layoutPopupAddCrypto.style.display = 'flex'
}
function noClosePopupAddCrypto () {
    noClose = true
}
function closePopupAddCrypto () {
    if (noClose == false) {
        layoutPopupAddCrypto.style.display = 'none'
    }
    noClose = false
}