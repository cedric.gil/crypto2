var btnEditCryptos = document.getElementById('btn_edit_cryptos')
var btnAppliCrypto = document.getElementById('btn_apply_crypto')
var inputsEditCrypto = document.getElementsByClassName('td_crypto_input')
var quantitiesCrypto = document.getElementsByClassName('td_crypto_quantity')
var onEdit = false

btnEditCryptos.addEventListener('click', editCryptos)

function editCryptos () {
    if (onEdit == false) {
        for (let i = 0; i < inputsEditCrypto.length; i++) {
            quantitiesCrypto.item(i).style.display = 'none'
            inputsEditCrypto.item(i).style.display = 'block'
        }
        btnEditCryptos.innerText = 'Annuler'
        btnAppliCrypto.style.display = 'inline'
        onEdit = true
    }
    else {
        for (let i = 0; i < inputsEditCrypto.length; i++) {
            inputsEditCrypto.item(i).style.display = 'none'
            quantitiesCrypto.item(i).style.display = 'block'
        }
        btnEditCryptos.innerText = 'Modifier'
        btnAppliCrypto.style.display = 'none'
        onEdit = false
    }

}