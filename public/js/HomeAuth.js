var linkConnect = document.getElementById('link_connect')
var linkRegister = document.getElementById('link_register')
var containerConnect = document.getElementById('container_connect')
var containerRegister = document.getElementById('container_register')

linkConnect.addEventListener('click', dispContainerConnect)
linkRegister.addEventListener('click', dispContainerRegister)

function dispContainerConnect () {
    linkRegister.style.color = 'rgba(49,47,47,0.65)'
    linkConnect.style.color = '#312F2F'
    containerRegister.style.opacity = '0%'
    containerRegister.style.display = 'none'
    containerConnect.style.opacity = '100%'
    containerConnect.style.display = 'table-row-group'
}
function dispContainerRegister () {
    linkConnect.style.color = 'rgba(49,47,47,0.65)'
    linkRegister.style.color = '#312F2F'
    containerConnect.style.opacity = '0%'
    containerConnect.style.display = 'none'
    containerRegister.style.opacity = '100%'
    containerRegister.style.display = 'table-row-group'
}