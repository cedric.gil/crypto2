<?php

/**
 * Class Engine
 */
class Engine
{

    /**
     * Injections des dépendences
     */
    public static function Api(){
        return Api::getInstance();
    }
    public static function Auth(){
        return Auth::getInstance(Database::getInstance(Config::getInstance()));
    }
    public static function Crypto(){
        return Crypto::getInstance(Database::getInstance(Config::getInstance()), Api::getInstance());
    }
    public static function Database(){
        return Database::getInstance(Config::getInstance());
    }
    public static function Session(){
        return Session::getInstance();
    }
    public static function Twig(){
        return Twig::getInstance();
    }

    /**
     * Authentifie un utilisateur
     * @param $user string
     * @param $password string
     * @return bool
     */
    public static function AuthLogin($user, $password){
        $user = strtolower($user);
        $res = self::Auth()->login($user, $password);
        if ($res == true) {
            self::Session()->setValue('user', $user);
        }
        return $res;
    }

    /**
     * Inscris un utilisateur
     * @param $user string
     * @param $email string
     * @param $password string
     * @param $cpassword string
     * @return string
     */
    public static function AuthRegister($user, $email, $password, $cpassword){
        $user = strtolower($user);
        $email = strtolower($email);
        $res = self::Auth()->register($user, $email, $password, $cpassword);
        if ($res == true) {
            self::Session()->setValue('user', $user);
            self::Crypto()->newPortfolio($user);
        }
        return $res;
    }

    /**
     * Retourne les tables HTML des cryptos
     * @return array
     */
    public static function CryptoGetTables(){
        return self::Crypto()->getHTMLTables(self::Session()->getValue('user'));
    }

    /**
     * Retourne la datalist des cryptos
     * @return string
     */
    public static function CryptoDatalist(){
        return '<tr>
            <td>
                <input name="symbol" list="cryptos" required>
                '.self::Crypto()->getDatalist().'
            </td>
        </tr>';
    }

    /**
     * Ajoute une crypto dans un portfolio
     * @param $symbol string
     * @param $quantity string
     */
    public static function CryptoAddPortfolio($symbol, $quantity){
        $symbol = strtoupper($symbol);
        self::Crypto()->addPortfolio($symbol, $quantity, self::Session()->getValue('user'));
    }

    /**
     * Met à jour les cryptos d'un portfolio
     * @param $post array
     */
    public static function CryptoEditPortfolio($post){
        self::Crypto()->editPortfolio($post, self::Session()->getValue('user'));
    }

}

?>