<?php

class Crypto
{

    /**
     * Singleton
     * @var Crypto
     */
    static private $_instance;

    /**
     * @var Database
     */
    private $db;

    /**
     * @var Api
     */
    private $api;

    /**
     * Singleton
     * @param Database $db
     * @param Api $api
     * @return Crypto
     */
    static public function getInstance($db, $api){
        if(is_null(self::$_instance)){
            self::$_instance = new Crypto($db, $api);
        }
        return self::$_instance;
    }

    /**
     * Crypto constructor.
     * @param Database $db
     * @param Api $api
     */
    private function __construct($db, $api){
        $this->db = $db;
        $this->api = $api;
    }

    /**
     * Crée un nouveau portefeuille
     * @param $user string
     */
    public function newPortfolio($user) {
        $id = $this->db->select("SELECT id FROM users WHERE user = '$user'", 'fetchAssoc')['id'];
        $query = 'CREATE TABLE portfolio_'.$user.'
        (
            iduser INT PRIMARY KEY NOT NULL
        );
        INSERT INTO portfolio_'.$user.' (iduser) VALUE ('.$id.');';
        $this->db->insert($query);
    }

    /**
     * Retourne les tables HTML des cryptos
     * @param $user string
     * @return array
     */
    public function getHTMLTables($user) {
        // Définition des variables
        $query = 'SELECT * FROM portfolio_'.$user;
        $portfolio = $this->db->select($query, 'fetchAssoc');
        $api = $this->api->getAllCryptos();
        $total_value = 0;
        $total_euro_24h = 0;
        $total_percent_24h = 0;
        $table_cryptos = '';
        $nb_neg = 0;
        $nb_pos = 0;
        $cnt = 0;

        // Construction des tables
        foreach ($portfolio as $symbol => $quantity) {
            if ($symbol != 'iduser') {
                $cnt++;
                $res = $this->getHTMLTables_Values($symbol, $api['cryptos'], $quantity);
                $total_value += $res['value'];
                $total_euro_24h += $res['euro_24h'];
                $total_percent_24h += $res['percent_24h'];
                if ($res['neg'] == true) {
                    $nb_neg++;
                } else {
                    $nb_pos++;
                }
                $values = [
                    'value' => $res['value'],
                    'euro_24h' => $res['euro_24h'],
                    'percent_24h' => $res['percent_24h']
                ];
                $table_cryptos .= $this->getHTMLTables_Cryptos($symbol, $quantity, $values);
            }
        }
        $table_total = $this->getHTMLTables_Total($cnt, $total_value, $total_euro_24h, $total_percent_24h, $api['cap_btc'], $api['cap_eth'], $api['total_cap'], $nb_neg, $nb_pos);

        return [
            'table_cryptos' => $table_cryptos,
            'table_total' => $table_total
        ];
    }
    // Calcul des valeurs d'une crypto
    private function getHTMLTables_Values ($symbol, $table_cryptos, $quantity) {
        // Récupération des informations de la crypto
        $i = 0;
        $ok = false;
        while ($ok == false) {
            if ($table_cryptos[$i]['symbol'] == $symbol) {
                $crypto = $table_cryptos[$i];
                $ok = true;
            }
            $i++;
        }

        // Calcul des valeurs
        $value = round($crypto['price'] * $quantity, 2);
        $euro_24h = round((($crypto['price'] / (100 + $crypto['percent_24h'])) * $crypto['percent_24h']) * $quantity, 2);
        $percent_24h = round($crypto['percent_24h'], 2);
        if ($percent_24h < 0) {
            $neg = true;
        } else {
            $neg = false;
        }

        return [
            'value' => $value,
            'euro_24h' => $euro_24h,
            'percent_24h' => $percent_24h,
            'neg' => $neg
        ];
    }
    // Création de la ligne HTML d'une crypto
    private function getHTMLTables_Cryptos ($symbol, $quantity, $values) {
        // Définition positif négatif
        if ($values['percent_24h'] < 0) {
            $class = 'neg';
            $prefix = '';
        }
        else {
            $class = 'pos';
            $prefix = '+';
        }

        return '<tr>
            <td>'.$symbol.'</td>
            <td class="td_crypto_quantity">'.$quantity.'</td>
            <td class="td_crypto_input"><input type="number" min="0" step=any name="'.$symbol.'_edit" value="'.$quantity.'"></td>
            <td>'.$values['value'].'€</td>
            <td class="'.$class.'">'.$prefix.$values['euro_24h'].'€</td>
            <td class="'.$class.'">'.$prefix.$values['percent_24h'].'%</td>
        </tr>';
    }
    // Création de la table HTML du total
    private function getHTMLTables_Total ($cnt, $total_value, $total_euro_24h, $total_percent_24h, $cap_btc, $cap_eth, $total_cap, $nb_neg, $nb_pos) {
        // Calcul des valeurs
        $cap_btc = round(($cap_btc * 100) / $total_cap, 2);
        $cap_eth = round(($cap_eth * 100) / $total_cap, 2);
        $total_cap = round($total_cap * 0.000000001, 1);
        if ($cnt > 0) {
            $total_percent_24h = round($total_percent_24h / $cnt, 2);
        }

        // Définition positif négatif
        if ($total_euro_24h < 0) {
            $class = 'neg';
            $prefix = '';
        }
        else {
            $class = 'pos';
            $prefix = '+';
        }

        return ['<tr>
            <td>'.$cnt.'<br><span id="ratio_neg">'.$nb_neg.'</span> / <span id="ratio_pos">'.$nb_pos.'</span></td>
            <td>'.$total_value.'€</td>
            <td class="euro_24h '.$class.'">'.$prefix.$total_euro_24h.'€</td>
            <td class="percent_24h '.$class.'">'.$prefix.$total_percent_24h.'%</td>
        </tr>',
        '<tr>
            <td colspan="2">'.$total_cap.'Md€</td>
            <td>'.$cap_btc.'%</td>
            <td>'.$cap_eth.'%</td>
        </tr>'];
    }

    /**
     * Retourne la datalist des cryptos
     * @return string
     */
    public function getDatalist () {
        $datalist = '<datalist id="cryptos">';
        foreach ($this->db->select('SELECT crypto FROM cryptos') as $crypto) {
            $datalist .= '<option value="' . $crypto['crypto'] . '">';
        }
        $datalist .= '</datalist>';
        return $datalist;
    }

    /**
     * Met à jour la liste des cryptos
     */
    public function updateCryptosList () {
        // Réinitialisation de la table
        $this->db->insert('DROP TABLE cryptos');
        $query = 'CREATE TABLE `crypto`.`cryptos` (
            `id` DECIMAL NOT NULL,
            `crypto` VARCHAR(255) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `id_UNIQUE` (`id` ASC));';
        $this->db->insert($query);

        // Insertion de la nouvelle liste
        foreach ($this->api->getAllCryptos()['cryptos'] as $key => $crypto) {
            $query = 'INSERT INTO cryptos VALUES (\'' . $key . '\', \'' . $crypto['symbol'] . '\')';
            $this->db->insert($query);
        }

        // Suppression des doublons
        $query = 'DELETE cryptos FROM cryptos
            LEFT OUTER JOIN (
                    SELECT MIN(id) as id, crypto
                    FROM cryptos
                    GROUP BY crypto
                ) as t1 
                ON cryptos.id = t1.id
            WHERE t1.id IS NULL';
        $this->db->insert($query);
    }

    /**
     * Ajoute une crypto dans un portfolio
     * @param $symbol string
     * @param $quantity string
     * @param $user string
     */
    public function addPortfolio ($symbol, $quantity, $user) {
        try {
            $query = 'UPDATE portfolio_' . $user . ' SET ' . $symbol . ' = ' . $symbol . ' + ' . $quantity;
            $this->db->insert($query);
        }
        catch (PDOException $ex) {
            $query = 'ALTER TABLE portfolio_' . $user . '
                ADD ' . $symbol . ' DOUBLE NOT NULL DEFAULT 0;
            UPDATE portfolio_' . $user . ' SET ' . $symbol . ' = ' . $symbol . ' + ' . $quantity . ';';
            $this->db->insert($query);
        }
    }

    /**
     * Met à jour toutes les cryptos d'un portfolio
     * @param $cryptos array
     * @param $user string
     */
    public function editPortfolio ($cryptos, $user) {
        foreach ($cryptos as $key => $value) {
            if ($key != 'edit_cryptos') {
                $symbol = str_replace('_edit', '', $key);
                if ($value > 0) {
                    $query = 'UPDATE portfolio_' . $user . ' SET ' . $symbol . ' = ' . $value;
                }
                else {
                    $query = 'ALTER TABLE portfolio_' . $user . ' DROP COLUMN ' . $symbol;
                }
                $this->db->insert($query);
            }
        }
    }

}

?>