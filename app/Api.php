<?php

class Api
{
    /**
     * Singleton
     * @var Api
     */
    private static $_instance;

    /**
     * @var stdClass
     */
    private $api;

    /**
     * Singleton
     * @return Api
     */
    public static function getInstance () {
        if(is_null(self::$_instance)){
            self::$_instance = new Api();
        }
        return self::$_instance;
    }

    /**
     * Api constructor.
     * CODE PRIS SUR LA DOCUMENTATION API DE COINMARKETCAP
     * https://coinmarketcap.com/api/documentation/v1
     * Retourne les informations relatives à toutes les cryptos
     */
    private function __construct () {
        $url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest';
        $parameters = [
            'start' => '1',
            'limit' => '5000',
            'convert' => 'EUR'
        ];
        $headers = [
            'Accepts: application/json',
            //'X-CMC_PRO_API_KEY: bc6aac6a-df61-4a9c-9f5f-51840c5e6fbb'
            'X-CMC_PRO_API_KEY: a04fe7a6-1526-4972-8855-82941e52a46b'
            //'X-CMC_PRO_API_KEY: 65ac2599-6a7c-4773-ac04-1e0eb9b5b46b'
            //'X-CMC_PRO_API_KEY: 4b3d6641-829e-4468-8d53-856b21cb39eb'
            //'X-CMC_PRO_API_KEY: d02c8637-a124-4a3e-8c5d-1b006e71d44e'
            //'X-CMC_PRO_API_KEY: b0ca455a-8a31-4675-b308-0a5d17fbf23c'

        ];
        $qs = http_build_query($parameters);
        $request = "{$url}?{$qs}";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $request,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false
        ));
        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        $this->api = $response;
        switch($this->api->status->error_code){
            case 1010:
                echo '<script>window.alert("Erreur API 1010 : La clé à atteint sa limite mensuelle de requêtes vers l\'API")</script>';
                exit(0);
            case 1011:
                echo '<script>window.alert("Erreur API 1011 : L\'adresse IP à atteint sa limite mensuelle de requêtes vers l\'API")</script>';
                exit(0);
        }
    }

    /**
     * Retourne toutes les infos actuelles nécessaires sur les cryptos
     * @return array
     */
    public function getAllCryptos () {
        // Initialisation des variables
        $cnt = 0;
        $cryptos = [];
        $total_cap = 0;

        // Récupération des informations
        foreach ($this->api->data as $crypto) {
            $cnt++;
            $cryptos = array_merge($cryptos, [[
                'symbol' => $crypto->symbol,
                'price' => $crypto->quote->EUR->price,
                'percent_24h' => $crypto->quote->EUR->percent_change_24h
            ]]);
            $total_cap += $crypto->quote->EUR->market_cap;
            if ($crypto->symbol == 'BTC') {
                $cap_btc = $crypto->quote->EUR->market_cap;
            }
            elseif ($crypto->symbol == 'ETH') {
                $cap_eth = $crypto->quote->EUR->market_cap;
            }
        }

        return [
            'cryptos' => $cryptos,
            'total_cap' => $total_cap,
            'cap_btc' => $cap_btc,
            'cap_eth' => $cap_eth
        ];
    }

}

?>