<?php

if (isset($_POST['connect'])) {
    $res = Engine::AuthLogin($_POST['user'], $_POST['password']);
    switch ($res) {
        case true:
            echo '<script>document.location = "?p=my"</script>';
            break;
        case false:
            $error = 'Le nom d\'utilisateur ou le mot de passe est incorrect';
            break;
        default:
            break;
    }
}
elseif (isset($_POST['register'])) {
    $res = Engine::AuthRegister($_POST['user'], $_POST['email'], $_POST['password'], $_POST['cpassword']);
    switch ($res) {
        case true:
            echo '<script>document.location = "?p=my"</script>';
            break;
        case 'exist':
            $error = 'Le nom d\'utilisateur ou l\'email est déjà utilisé';
            break;
        case 'low':
            $error = 'Le mot de passe n\'est pas assez robuste';
            break;
        case 'not_same':
            $error = 'Les deux mots de passe ne sont pas identiques';
            break;
        case 'same':
            $error = 'Le nom d\'utilisateur ne peut pas être identique au mot de passe';
            break;
        default:
            break;
    }
}

if (isset($error)) {
    echo '<script>window.alert("'.$error.'")</script>';
}

?>