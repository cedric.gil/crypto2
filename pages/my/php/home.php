<?php

$tables = Engine::CryptoGetTables();
$params = [
    'table_total' => $tables['table_total'],
    'table_cryptos' => $tables['table_cryptos'],
    'datalist_cryptos' => Engine::CryptoDatalist()
];

if (isset($_POST['add_crypto'])) {
    Engine::CryptoAddPortfolio($_POST['symbol'], $_POST['quantity']);
    echo '<script>document.location = "?p=my"</script>';
}
elseif (isset($_POST['edit_cryptos'])) {
    Engine::CryptoEditPortfolio($_POST);
    echo '<script>document.location = "?p=my"</script>';
}

?>